
return {
  full_name = "Dwarf",
  traits = {
    {
      name = "Ability Score Increase",
      effects = {
        { type = "ability_score_change", which = "CON", value = 2 }
      }
    },
    {
      name = "Dwarven Combat Training",
      effects = {
        { type = "proficiency", which = { "battleaxe", "handaxe",
                                          "light hammer", "warhammer" } }
      }
    }
  }
}

