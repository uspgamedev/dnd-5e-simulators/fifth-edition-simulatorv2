
extern crate lua;
use fes::effect::EffectKeeper;
use luawrapper;

struct Feature {
    name: String,
    fx_keeper: EffectKeeper,
}

pub struct Class {
    id: String,
    name: String,
    feature_table: Vec<Vec<Feature>>,
}

pub struct Level<'a> {
    class: &'a Class,
    level: u8,
    fx_keeper: EffectKeeper,
}

fn new(id: String, name: String, feature_table: Vec<Vec<Feature>>) -> Class {
    Class {
        id: id,
        name: name,
        feature_table: feature_table,
    }
}

impl PartialEq for Class {
    fn eq(&self, other: &Class) -> bool {
        self.id == other.id
    }
}

pub fn new_level<'a>(class: &'a Class, level: u8) -> Level<'a> {
    let mut class_level = Level {
        class: class,
        level: level,
        fx_keeper: EffectKeeper::new(),
    };
    for feature in class.feature_table[(level - 1) as usize].iter() {
        class_level.fx_keeper.merge(&feature.fx_keeper);
    }
    if class_level.fx_keeper.has_hit_dice() {
        let dice = class_level.fx_keeper.get_hit_dice();
        let amount = match level == 1 {
            true => dice.sides(),
            false => dice.roll(),
        };
        class_level.fx_keeper.set_hit_points(amount);
    }
    class_level
}

impl<'a> Level<'a> {
    pub fn get_class(&self) -> &Class {
        self.class
    }
    pub fn get_hit_points(&self) -> u8 {
        self.fx_keeper.hit_points()
    }
}

pub fn from_script(id: String, filename: &'static str) -> Class {
    let mut wrapper = luawrapper::wrapper::new(lua::State::new());
    wrapper.load_file(filename);
    wrapper.step_into_call_result();
    let name = wrapper.get_field::<&str, String>("name");
    wrapper.step_into_field("feature_table");
    let mut feature_table = Vec::<Vec<Feature>>::new();
    let len = wrapper.get_len();
    for i in 1..len + 1 {
        wrapper.step_into_field(i);
        let mut features = Vec::<Feature>::new();
        let features_len = wrapper.get_len();
        for j in 1..features_len + 1 {
            wrapper.step_into_field(j);
            let name = wrapper.get_field::<&str, String>("name");
            wrapper.step_into_field("effects");
            let fx_len = wrapper.get_len();
            let mut fx_keeper = EffectKeeper::new();
            for k in 1..fx_len + 1 {
                wrapper.step_into_field(k);
                let effect_type = wrapper.get_field::<&str, String>("type");
                if effect_type == "hit_dice" {
                    let sides = wrapper.get_field::<&str, i64>("sides") as u8;
                    fx_keeper.set_hit_dice(sides, id.clone());
                }
                wrapper.step_back();
            }
            let class_feature = Feature { name, fx_keeper };
            features.push(class_feature);
            wrapper.step_back();
            wrapper.step_back();
        }
        feature_table.push(features);
        wrapper.step_back();
    }
    wrapper.step_back();
    return new(id.clone(), name, feature_table);
}
