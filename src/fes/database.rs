
extern crate lua;

use std::collections::HashMap;
use fes::race;
use fes::class;

struct Database {
    races: HashMap<String, race::Race>,
    classes: HashMap<String, class::Class>,
    state: lua::State,
}

pub struct DatabaseProxy {
    db: Database,
}

pub fn new() -> DatabaseProxy {
    let mut db = Database {
        races: HashMap::new(),
        classes: HashMap::new(),
        state: lua::State::new(),
    };
    db.races.insert(String::from("dwarf"),
                    race::from_script("scripts/races/dwarf.lua"));
    db.classes.insert(String::from("fighter"),
                      class::from_script(String::from("fighter"), "scripts/classes/fighter.lua"));
    DatabaseProxy { db: db }
}

impl DatabaseProxy {
    pub fn get_race(&self, name: String) -> &race::Race {
        self.db
            .races
            .get(&name)
            .unwrap()
    }
    pub fn get_class(&self, id: String) -> &class::Class {
        self.db
            .classes
            .get(&id)
            .unwrap()
    }
}
