
use fes::character::AbilityScoreType;
use fes::dice::Dice;

#[derive(Clone)]
struct AbilityScoreChange {
    which_ability: AbilityScoreType,
    value: i8,
}

#[derive(Clone)]
struct HitDice {
    class_identifier: String,
    dice: Dice,
}

pub struct EffectKeeper {
    ability_score_changes: Vec<AbilityScoreChange>,
    hit_dice: Option<HitDice>,
    hit_points: u8,
}

impl EffectKeeper {
    pub fn new() -> EffectKeeper {
        EffectKeeper {
            ability_score_changes: Vec::<AbilityScoreChange>::new(),
            hit_dice: None,
            hit_points: 0,
        }
    }

    pub fn merge(&mut self, other: &EffectKeeper) {
        self.ability_score_changes.extend(other.ability_score_changes.iter().cloned());
        if self.hit_dice.is_some() && other.hit_dice.is_some() {
            panic!();
        } else if other.hit_dice.is_some() {
            self.hit_dice = other.hit_dice.clone();
        }

    }

    pub fn total_ability_score_modifier(&self, which: &AbilityScoreType) -> i8 {
        self.ability_score_changes
            .iter()
            .filter(|&x| x.which_ability == *which)
            .fold(0, |acc, x| acc + x.value)
    }

    pub fn add_ability_score_modifier(&mut self, which: &AbilityScoreType, value: i8) {
        self.ability_score_changes.push(AbilityScoreChange {
                                            which_ability: which.clone(),
                                            value: value,
                                        });
    }

    pub fn has_hit_dice(&self) -> bool {
        self.hit_dice.is_some()
    }

    pub fn set_hit_dice(&mut self, sides: u8, class_identifier: String) {
        self.hit_dice = Some(HitDice {
                                 class_identifier: class_identifier,
                                 dice: Dice::new(sides),
                             });
    }

    pub fn get_hit_dice(&self) -> Dice {
        self.hit_dice
            .clone()
            .unwrap()
            .dice
    }

    pub fn set_hit_points(&mut self, amount: u8) {
        self.hit_points = amount;
    }

    pub fn hit_points(&self) -> u8 {
        self.hit_points
    }
}
