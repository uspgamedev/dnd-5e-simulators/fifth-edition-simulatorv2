
extern crate lua;

use std::cmp::min;
use fes::race::Race;
use fes::class::Class;
use fes::class::Level;
use fes::class::new_level;
use fes::database::DatabaseProxy;
use luawrapper;

#[derive(PartialEq, Clone)]
pub enum AbilityScoreType {
    STR,
    DEX,
    CON,
    INT,
    WIS,
    CHA,
}

impl AbilityScoreType {
    pub fn from_string(name: &str) -> AbilityScoreType {
        match name {
            "STR" => AbilityScoreType::STR,
            "DEX" => AbilityScoreType::DEX,
            "CON" => AbilityScoreType::CON,
            "INT" => AbilityScoreType::INT,
            "WIS" => AbilityScoreType::WIS,
            "CHA" => AbilityScoreType::CHA,
            _ => panic!("Unknown ability score type"),
        }
    }
}

pub struct Character<'a> {
    name: String,
    ability_scores: [u8; 6],
    race: &'a Race,
    levels: Vec<Level<'a>>,
    damage: u8,
}

impl<'a> Character<'a> {
    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn total_level(&self) -> u8 {
        self.levels.len() as u8
    }

    pub fn get_hp(&self) -> u8 {
        self.get_max_hp() - self.damage
    }

    pub fn get_max_hp(&self) -> u8 {
        let con = self.ability_score_modifier(&AbilityScoreType::CON);
        let mut total = self.total_level() as i8 * con;
        self.levels.iter().fold(total as u8, |acc, lv| acc + lv.get_hit_points())
    }

    pub fn take_damage(&mut self, amount: u8) {
        self.damage = min(self.get_max_hp(), self.damage + amount)
    }

    pub fn ability_score(&self, which: &AbilityScoreType) -> u8 {
        let score = self.ability_scores[which.clone() as usize] as i8;
        (score + self.race.total_ability_score_modifier(which)) as u8
    }

    pub fn ability_score_modifier(&self, which: &AbilityScoreType) -> i8 {
        ((self.ability_score(which) as i8) - 10) / 2
    }

    pub fn increase_level(&mut self, class: &'a Class) {
        let total = self.levels
            .iter()
            .filter(|&x| x.get_class() == class)
            .fold(0, |acc, x| acc + 1);
        self.levels.push(new_level(class, total + 1));
    }

    pub fn print(&self, verbose: bool) {
        println!("{}", self.name());
        println!("Hit Points: {}/{}", self.get_hp(), self.get_max_hp());
        if verbose {
            println!("STR: {}", self.ability_score(&AbilityScoreType::STR));
            println!("DEX: {}", self.ability_score(&AbilityScoreType::DEX));
            println!("CON: {}", self.ability_score(&AbilityScoreType::CON));
            println!("INT: {}", self.ability_score(&AbilityScoreType::INT));
            println!("WIS: {}", self.ability_score(&AbilityScoreType::WIS));
            println!("CHA: {}", self.ability_score(&AbilityScoreType::CHA));
        }
    }
}

pub fn new<'a>(name: String, ability_scores: [u8; 6], race: &'a Race) -> Character<'a> {
    Character {
        name: name,
        ability_scores: ability_scores,
        race: race,
        levels: Vec::<Level<'a>>::new(),
        damage: 0,
    }
}

pub fn from_script<'a>(db: &'a DatabaseProxy, filename: &'static str) -> Character<'a> {
    let mut wrapper = luawrapper::wrapper::new(lua::State::new());
    wrapper.load_file(filename);
    wrapper.step_into_call_result();
    let name = wrapper.get_field::<&str, String>("name");
    let abscores_vec = wrapper.get_vec::<&'static str, i64>("ability_scores");
    let abscores = [abscores_vec[0] as u8,
                    abscores_vec[1] as u8,
                    abscores_vec[2] as u8,
                    abscores_vec[3] as u8,
                    abscores_vec[4] as u8,
                    abscores_vec[5] as u8];
    let race_name = wrapper.get_field::<&str, String>("race");
    let race = db.get_race(race_name);
    let mut character = new(name, abscores, race);
    let levels = wrapper.get_vec::<&str, String>("levels");
    for class_id in levels.iter() {
        character.increase_level(db.get_class(class_id.clone()));
    }
    return character;
}
